import Actions from "./patient-action";

const initialState = {
    appointments: [],
    patients:[],
    adddepartments :[],
    departmentsList : [],
    doctorsList:[],
    removedPatients:[],
    bookedAppointmentsNotify:[]
   
};

const patientsData = (state = initialState, action) => {
    switch (action.type) {
        case Actions.GET_APPOINTMENTS:
            return { ...state, appointments: action.payload };
        case Actions.GET_PATIENTS:
            return { ...state, patients: action.payload };
        case Actions.SET_DEPARTMENTS:
            return { ...state, adddepartments: action.payload };
        case Actions.GET_DEPARTMENTS_LIST:
            return { ...state, departmentsList: action.payload };
        case Actions.DOCTORS_LIST:
            return { ...state, doctorsList: action.payload };
        case Actions.GET_DELETE_PATIENTS:
            return { ...state, removedPatients: action.payload };
        case Actions.GET_BOOK_APPOINTMENTS:
            return { ...state, bookedAppointmentsNotify: action.payload };
            
        default:
        return state;
    }
};

export default patientsData;
