import React from 'react';
import ReactDOM from 'react-dom';
import Router from './routes/router';
import "bootstrap/dist/css/bootstrap.min.css";
import configureStore from './configureStore';
import {Provider} from 'react-redux';

const store = configureStore();
ReactDOM.render
(<Provider store={store}><Router> </Router></Provider>, document.getElementById('root'));
