import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Actions from '../../data/patient-action';
import { Bar , Line } from 'react-chartjs-2';



function PatientsByDepartment () {
    const departmentList = useSelector(state => state.PatientsData.departmentsList);
    const patients = useSelector(state => state.PatientsData.patients);
    const doctorsList = useSelector(state => state.PatientsData.doctorsList);
    const dispatch = useDispatch();


        useEffect(() => {
        dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
        dispatch({ type: Actions.DISPLAY_DOCTORS });
        dispatch({ type: Actions.POST_PATIENTS });
    }, [])



const patientsPerDepartment =()=>{
    return doctorsList.map(({doctorname}) => patients.reduce(function(accumulator, patient) {
        if(patient.doctorname === doctorname) {
            accumulator++;
        }            
        return accumulator;
    }, 0));
}

const data = {
    labels: doctorsList.map(doctor => doctor.doctorname),        
    datasets: [{
        label: "Patients",
        backgroundColor: [
            '#fb3b01',
            '#a8f3f3',
            '#c412e2',
            '#ffc107',
            '#dc0f00'
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            
        ],
        data: patientsPerDepartment(),
    }]
}

    return (
        <div>
            <Line data={data}/>
        </div>
    )
}

export default PatientsByDepartment;