import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Form, ListGroup, Carousel } from 'react-bootstrap';
import Actions from '../data/patient-action';
import "../styles/dashboard.sass";
import Back from './backbutton/back';
import DashboardCards from '../components/forms/dashboardCards';
import { Bar , Line } from 'react-chartjs-2';
import Button from './forms/button';
import Modal from './forms/modal';
import Input from './forms/input';
import Carousels from '../components/forms/carousel';
import Barchart from './charts/barchart';
import Linechart from './charts/linechart';
import Piechart from './charts/piechart';
function Dashboard() {
    const dispatch = useDispatch();
    const departmentStatus = useSelector(state => state.PatientsData.adddepartment);
    const departmentList = useSelector(state => state.PatientsData.departmentsList);
    const doctorsList = useSelector(state => state.PatientsData.doctorsList);
    const patients = useSelector(state => state.PatientsData.patients);
    const [index, setIndex] = useState(0);

    const [departmentModal, setDepartmentModel] = useState();
    const [department, setDepartment] = useState();
    const [deptDescription, setDeptDescription] = useState();
    const [doctorModal, setDoctorModel] = useState();
    const [doctorName, setDoctorName] = useState();
    const [doctorDepartment, setDoctorDepartment] = useState();
    const [mobile, setMobile] = useState();
    const [email, setEmail] = useState();

    useEffect(() => {
        dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
        dispatch({ type: Actions.DISPLAY_DOCTORS });
        dispatch({ type: Actions.POST_PATIENTS });
    }, [])


    const _addDepartment = () => {
        setDepartmentModel(true);
    }

    const _addDoctor = () => {
        setDoctorModel(true);
    }

    const _onFormSubmit = (e) => {
        e.preventDefault();
        const payload = { department: department, description: deptDescription }
        dispatch({ type: Actions.POST_DEPARTMENTS, payload: payload });

        setDepartmentModel(false);
    }

    const _onDoctorFormSubmit = (e) => {
        e.preventDefault();
        const payload = { doctorname: doctorName, doctordepartment: doctorDepartment, mobile: mobile, email: email }

        dispatch({ type: Actions.POST_DOCTORS, payload: payload });
        setDoctorModel(false);
    }

    const _departmentsList = () => {
        return (
            <div>
                {departmentList.map((dept, index) => {
                    return (

                        <ListGroup variant="flush"><ListGroup.Item key={index}>{dept.department}</ListGroup.Item></ListGroup>
                    )
                })}

            </div>
        )
    }

    const _doctorsList = () => {
        return (
            <div>
                {doctorsList.map((doctor, index) => {
                    return (

                        <ListGroup variant="flush"><ListGroup.Item key={index}>{doctor.doctorname} - {doctor.doctordepartment}</ListGroup.Item></ListGroup>
                    )
                })}

            </div>
        )
    }

    const _deptFields = () => {
        return (<div>
            <Form onSubmit={_onFormSubmit}>
                <Input fieldname="Department" placeholder="Enter Department" name="department" onChange={e => setDepartment(e.target.value)} />
                <Input fieldname="Description" placeholder="Enter Department Description" name="description" onChange={e => setDeptDescription(e.target.value)} />
                <Button type="submit" name="Submit" />
            </Form>
        </div>
        )
    }


    const _doctorFields = () => {
        return (
            <Form onSubmit={_onDoctorFormSubmit}>
                <Input fieldname="Doctor Name" placeholder="Enter Doctor Name" name="doctorname" onChange={e => setDoctorName(e.target.value)} />
                {/* <Input fieldname="Department" placeholder="Enter Department" name="department" onChange={e => setDoctorDepartment(e.target.value)} /> */}
                <Input type="number" fieldname="Mobile No." placeholder="Enter Mobile Number" name="mobile" onChange={e => setMobile(e.target.value)} />
                <label>Choose Department</label>
                    <select className="ui dropdown fluid" name="department" onChange={e => setDoctorDepartment(e.target.value)}  value={doctorDepartment}>
                        <option value="0">Select Department</option>
                        {departmentList.map((dept, index) => {
                            return ( <option key={index} value={dept.department}> {dept.department} </option> );
                        })}
                    </select>
                <Input type="email" fieldname="Email" placeholder="Enter Email Id" name="email" onChange={e => setEmail(e.target.value)} />
                <Button type="submit" name="Submit" />
            </Form>
        )
    }

    
    return (
        <div className="dashboard">
            {departmentModal && <Modal show={departmentModal} onHide={() => setDepartmentModel(false)} heading="Add Department" content={_deptFields()}></Modal>}
            {doctorModal && <Modal show={doctorModal} onHide={() => setDoctorModel(false)} heading="Add Doctor" content={_doctorFields()}></Modal>}
            <Back ></Back>
            <div className="dashboardContents">
                <Row className="row">
                <Col xs={4}>
                    <DashboardCards bg="light" text="black" className="DbCards" header="Month Wise Appointments" content={<Piechart/>}/>
                    </Col>
                    <Col xs={4}>
                        <DashboardCards bg="light" text="black" className="DbCards" header="Patients Visited Per Doctor" content={<Linechart/>} />
                    </Col>

                    <Col xs={4}>
                        <DashboardCards bg="light" text="black" className="DbCards" header=" Patients Visited Per Departmanet" content={<Barchart />} />
                    </Col>
                </Row>
                {/* <Row>
                    
                    <Col xs={4}>
                        <DashboardCards bg="info" text="white" header="Revenue Summary" />
                    </Col>
                    <Col xs={4}>
                        <DashboardCards bg="warning" text="white" header="Patients Consulted Doctor" />
                    </Col>

                </Row> */}
                <Row>
                <Col xs={4}>
                        <DashboardCards bg="light" text="black"  header="Patient Visited Summary & Revenue Summary" className="cardBody DbCards" content={<Carousels />} />
                    </Col>
                    <Col xs={4}>
                        <DashboardCards bg="light" text="black"   header="Doctors List & Departments" className="deptcard DbCards" addbtn={<Button name="Add New Doctor" onClick={_addDoctor} />} content={<_doctorsList />} />
                    </Col>
                    <Col xs={4}>
                        <DashboardCards bg="light" text="black"  header=" Departments List" className="deptcard DbCards" addbtn={<Button name="Add New Department" onClick={_addDepartment} />} content={<_departmentsList />} />
                    </Col>

                </Row>
            </div>
        </div>
    )
}

export default Dashboard;