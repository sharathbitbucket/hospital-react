import React from 'react';
import "../styles/landing.sass";
import { Container, Row, Col, Button } from "react-bootstrap";
import { FaPenAlt } from 'react-icons/fa';
import { useHistory } from 'react-router-dom';
import Cards from "./forms/card";
import {FaWheelchair, FaDollarSign, FaStethoscope } from 'react-icons/fa';
function App() {

    const history = useHistory();

    const _handleClick = () => {
        history.push("/appointments");
    }
    const _patientsPage=()=>
    {
        history.push("/patients");
    }
    const _doctorsPage=()=>
    {
        history.push("/doctors");
    }
    const _paymentsPage=()=>
    {
        history.push("/payments");
    }
    return (
        <div className="landingPage">
            <Container >
                <Row>
                    <Col xs={4}><p className="landingtext2">Stay Healthy...Be Strong!!!</p></Col>
                    <Col xs={4}></Col>
                    <Col xs={4}></Col>
                </Row>
                <Row>
                    <Col xs={4}><Button variant="primary" size="lg" block className="appointmentbtn" onClick={_handleClick}><FaPenAlt /> Book an Appointment</Button></Col>
                </Row>

            </Container>
            <Row className="lastrow">
                <Container>
                    <Row>
                        <Col xs={4}>
                            <Cards header="Patients" onClick ={_patientsPage} headerclass="headerclass" metaclass="metaclass" descclass="descclass" icon={<FaWheelchair/>} meta="Register & Book an Appointment" description="Click Here"/>
                        </Col>
                        <Col xs={4}>
                            <Cards header="Doctors" onClick ={_doctorsPage} headerclass="headerclass" metaclass="metaclass" descclass="descclass" icon={<FaStethoscope/>} meta="Consult & Get the Solution" description="Click Here"/>
                        </Col>
                        <Col xs={4}>
                            <Cards header="Payments" onClick ={_paymentsPage} headerclass="headerclass" metaclass="metaclass" descclass="descclass" icon={<FaDollarSign/>} meta="Less Pay & More Help" description="Click Here"/>
                        </Col>
                    </Row>
                </Container>
            </Row>
        </div>
            )
        }
        
export default App;