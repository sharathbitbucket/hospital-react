import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Actions from "../data/patient-action";
import Tables from "./forms/table";
import Modals from "./forms/modal";
import {
  Container,
  Row,
  Col,
  Card,
  Accordion,
  Button,
  Form,
  FormGroup,
  Modal,
  Table,
} from "react-bootstrap";
import BackButton from "./backbutton/back";
import {
  FaRegTrashAlt,
  FaEdit,
  FaCommentsDollar,
  FaCalendarCheck,
} from "react-icons/fa";
import "../styles/patients.sass";
import Moment from "react-moment";
import Input from "./forms/input";
import Buttons from "./forms/button";
import EditableForm from "./editablePatients";
import Appointments from "./bookAppointments";
import Search from "./forms/search";

function Patients() {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const today = new Date();
  const departmentList = useSelector(
    (state) => state.PatientsData.departmentsList
  );
  const doctorsList = useSelector((state) => state.PatientsData.doctorsList);
  const dispatch = useDispatch();
  const patientsDetails = useSelector((state) => state.PatientsData.patients);
  const deletedPatientsSuccess = useSelector(
    (state) => state.PatientsData.removedPatients
  );
  const [activePatient, setActivePatient] = useState(null);
  const [editablePatientsData, setEditablePatientsData] = useState({});
  const [bookAppointmentsModal, setBookAppointmentsModal] = useState(false);
  const [bookedAppointments, setBookedAppointments] = useState();
  const [editPatientModal, setEditPatientModal] = useState();
  const [expansion, setExpansion] = useState(false);
  const [deletedSuccessModal, setDeletedSuccessModal] = useState(false);
  const [expandData, setExpandData] = useState();
  const [patientSearchValue, setPatientSearchValue] = useState();
  const [patientsFilteredDetails, setPatientsFilteredDetails] = useState();

  useEffect(() => {
    dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
    dispatch({ type: Actions.DISPLAY_DOCTORS });
    dispatch({ type: Actions.POST_PATIENTS });
  }, []);

  const _handleEdit = (patients) => {
    setEditablePatientsData(patients);
    setEditPatientModal(!editPatientModal);
  };

  const _handleBookAppointment = (patients) => {
    setBookAppointmentsModal(!bookAppointmentsModal);
    setActivePatient(patients);
    // setBookedAppointments(patients);
  };

  const _handleDelete = (patients) => {
    dispatch({ type: Actions.DELETE_PATIENTS, payload: patients.id });
    setDeletedSuccessModal(!deletedSuccessModal);
  };

  const _handleClose = () => {
    setDeletedSuccessModal(!deletedSuccessModal);
    dispatch({ type: Actions.POST_PATIENTS });
  };

  const AlertComponent = () => {
    return (
      <Modal show={deletedSuccessModal} onHide={_handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Patients</Modal.Title>
        </Modal.Header>
        <Modal.Body>{deletedPatientsSuccess.data}</Modal.Body>
      </Modal>
    );
  };

  const _tableHead = () => {
    return (
      <>
        <th>#</th>
        {/* <th>Patient ID</th> */}
        <th>Date</th>
        <th>Patient Name</th>
        <th>Gender</th>
        <th>Age</th>
        {/* <th>Department</th>
                <th>Consult Doctor</th> */}
        <th>Mobile No.</th>
        <th>Email</th>
        <th>Action</th>
      </>
    );
  };

  const getNumberOfPatientsByMonth = (inputMonth) => {
    const patientsByMonth = patientsDetails.filter(({ date }) => {
      const dateObject = new Date(date);
      const currentMonth = dateObject.getMonth();
      if (currentMonth === inputMonth) {
        return true;
      } else {
        return false;
      }
    });
    const patientCount = patientsByMonth.length;
    return patientCount;
  };

  const editedPatients = (name, value) => {
    setEditablePatientsData({ ...editablePatientsData, [name]: value });
  };

  const _bookAppointment = (name, value) => {
    setBookedAppointments({ ...bookedAppointments, [name]: value });
  };

  const _handlePatientSearchChange = (e) => {
    setPatientSearchValue(e.target.value);
  };
  const _handlePatientSearchClick = (e) => {
    e.preventDefault();
    const SearchedPatients = patientsDetails.filter((patientData) => {
      if (patientData.name === patientSearchValue) {
        return true;
      } else {
        return false;
      }
    });
    setPatientsFilteredDetails(SearchedPatients);
    console.log(SearchedPatients);
  };
  const _expandedTableHeader = () => {
    return (
      <>
        <th>Date</th>
        <th>Department</th>
        <th>Doctor Name</th>
      </>
    );
  };
  const _expandedTableBody = (index) => {
    return (
      <tr>
        <td> {expandData.date}</td>
        <td> {expandData.department}</td>
        <td> {expandData.doctorname}</td>
      </tr>
    );
  };
  const _expandData = (patients) => {
    return (
      <>
        <Tables thead={_expandedTableHeader()} tbody={_expandedTableBody()} />
      </>
    );
  };

  const _handleExpand = (patients) => {
    if (expansion === patients.id) {
      setExpansion("");
    } else {
      setExpansion(patients.id);

      setExpandData(patients);
    }
  };
  const getNumberOfPatientsByDay = (inputDay) => {
    const patientsByDay = patientsDetails.filter(({ date }) => {
      const dateObject = new Date(date);
      const currentMonth = dateObject.getDay();
      if (currentMonth === inputDay) {
        return true;
      } else {
        return false;
      }
    });
    const patientCount = patientsByDay.length;
    return patientCount;
  };

  const _tableBody = (listOfPatients) => {
    return (
      <>
        {listOfPatients.map((patients, index) => {
          return (
            <>
              <tr key={index} onClick={() => _handleExpand(patients)}>
                <td> {index + 1}</td>
                {/* <td>PT0{index + 1}</td> */}
                <td> {patients.date}</td>
                <td> {patients.name}</td>
                <td>{patients.gender} </td>
                <td> {patients.age}</td>
                {/* <td>{patients.department} </td>
                <td>{patients.doctorname} </td> */}
                <td> {patients.mobile}</td>
                <td> {patients.email}</td>
                <td>
                  {" "}
                  <Row>
                    <Col
                      sm={4}
                      style={{ color: "green" }}
                      onClick={() => _handleBookAppointment(patients)}
                      title="Book Appointment"
                    >
                      <FaCalendarCheck />
                    </Col>
                    <Col
                      sm={4}
                      style={{ color: "blue" }}
                      onClick={() => _handleEdit(patients)}
                    >
                      <FaEdit title="Edit" />
                    </Col>
                    <Col
                      style={{ color: "red" }}
                      sm={4}
                      onClick={() => _handleDelete(patients)}
                      title="Delete"
                    >
                      <FaRegTrashAlt />
                    </Col>
                  </Row>
                </td>
              </tr>
              {expansion === patients.id && (
                <tr>
                  <_expandData />
                </tr>
              )}
            </>
          );
        })}
      </>
    );
  };

  return (
    <div className="patientsPage">
      {deletedSuccessModal && <AlertComponent />}
      {editPatientModal && (
        <Modals
          show={editPatientModal}
          onHide={() => setEditPatientModal(false)}
          heading="Edit and Update Patients Data"
          content={
            <EditableForm
              patients={editablePatientsData}
              onChange={editedPatients}
            />
          }
        ></Modals>
      )}
      {bookAppointmentsModal && (
        <Modals
          show={bookAppointmentsModal}
          onHide={() => setBookAppointmentsModal(false)}
          heading="Book New Appointment"
          content={
            <Appointments
              patientData={activePatient}
              appointments={bookAppointmentsModal}
              onChange={_bookAppointment}
            />
          }
        ></Modals>
      )}
      <BackButton className="backButton"></BackButton>
      <div className="patientsRow">
        <br />
        <Card.Header className="summaryHeader" as="h4">
          <Row className="">
            <Col xs={6}>Patient's Summary </Col>
            <Col xs={6}>
              {" "}
              <Search
                name="patientSearch"
                placeholder="Type Patient to Search..."
                onChange={_handlePatientSearchChange}
                value={patientSearchValue}
                onClick={_handlePatientSearchClick}
              />{" "}
            </Col>
          </Row>
        </Card.Header>
        <Row className="">
          <Col xs={3}>
            <Card className="">
              <Card.Body>
                <Card.Header className="summaryHeader">
                  {" "}
                  Summary of This {monthNames[today.getMonth()]}
                </Card.Header>
                <Row>
                  <Col xs={8}>
                    <p className="description">
                      No.of Patients Visited So far in This{" "}
                      {monthNames[today.getMonth()]}
                    </p>
                  </Col>
                  <Col xs={4}>
                    {" "}
                    <p className="patientsCount">
                      {getNumberOfPatientsByMonth(today.getMonth())}
                    </p>
                  </Col>
                </Row>
                <br />
                <Card.Header className="summaryHeader">
                  {" "}
                  Summary of Today
                </Card.Header>
                <Row>
                  <Col xs={8}>
                    <p className="description">
                      No.of Patients Visited Today are
                    </p>
                  </Col>
                  <Col xs={4}>
                    {" "}
                    <p className="patientsCount">
                      {getNumberOfPatientsByDay(today.getDay())}
                    </p>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Col>

          <Col xs={9}>
            <Card className="patients-card">
              <Card.Body>
                {!patientsFilteredDetails ? (
                  <Tables
                    thead={_tableHead()}
                    tbody={_tableBody(patientsDetails)}
                  />
                ) : (
                  <Tables
                    thead={_tableHead()}
                    tbody={_tableBody(patientsFilteredDetails)}
                  />
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Patients;
