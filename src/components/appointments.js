import React, { useState, useEffect, useForm } from 'react';
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import Input from './forms/input';
import Actions from '../data/patient-action';
import { Form, Dropdown, Card, FormGroup, Container, Row, Col, Button, Modal } from "react-bootstrap";
import Buttons from './forms/button';
import DropDowns from './forms/dropdown';
import Back from './backbutton/back';
import "../styles/appointments.sass";
import { FaListOl } from 'react-icons/fa';
import { $ } from "jquery";
import formInputValidations from './validations/validation';

function Appointments() {
   
    const [patientDetails, setPatientDetails] = useState({});
    const [modalOpenClose, setModalOpenClose] = useState(false);
    const [department, setDepartment] = useState();
    const [filterDoctor, setFilterDoctor] = useState();
    const dispatch = useDispatch();
    const history = useHistory();
    const appointmentsResponse = useSelector(state => state.PatientsData.appointments);
    const departments = useSelector(state => state.PatientsData.departmentsList);
    const doctors = useSelector(state => state.PatientsData.doctorsList);
    ;
    const initialState = {
        name: "",
        gender: "",
        age: "",
        mobile: "",
        address: "",
        department: "Select Department",
        doctorname: "Select Doctor to Consult",
        email: ""
    };

    const errorMessages = {
        name: "",
        gender: "",
        age: "",
        mobile: "",
        address: "",
        department: "",
        doctorname: "",
        email: ""
    }
    const [appointmentErrors, setAppointmentErrors] = useState(errorMessages);

    useEffect(() => {
        //To get the present date onloading of page
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;

        setPatientDetails({ ...patientDetails, date: today });
        dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
        dispatch({ type: Actions.DISPLAY_DOCTORS });

    }, []);

    const onFormSubmit = (e) => {
        e.preventDefault();
        dispatch({ type: Actions.POST_APPOINTMENTS, payload: patientDetails });        
        setModalOpenClose(!modalOpenClose);
        history.push("/patients");
    }

    const onChange = (target) => {       
        
        const { name, value } = target;
        let errMessage = formInputValidations(name, value);       
        setAppointmentErrors({...appointmentErrors, [name]: errMessage});
        setPatientDetails({ ...patientDetails, [name]: value, department: department });
       
    }
    const _handleClose = () => {
        setModalOpenClose(!modalOpenClose);
        setPatientDetails(initialState);
    }
    const AlertComponent = () => {
        return (
            <Modal show={modalOpenClose} onHide={_handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Appointments</Modal.Title>
                </Modal.Header>
                <Modal.Body>{appointmentsResponse.data}</Modal.Body>
            </Modal>
        )
    }
    const _handleClick = () => {
        history.push("/patients");
    }
    const _handleDepartmentChange = (e) => {
        e.preventDefault();
        setDepartment(e.target.value);

        setFilterDoctor(doctors.filter(({ doctordepartment }) => doctordepartment === e.target.value));

    }
    const _gender = () => {
        return (
            <div>
                <Form.Control as="select" name="gender" onChange={e => onChange(e.target)} value={patientDetails.gender}>
                    <option value="">Select Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </Form.Control>
            </div>
        )
    }

    return (
        <div className="appointmentsPage">
            {modalOpenClose && <AlertComponent />}
            <Back className="backButton"></Back>
            <Row>
                <Col xs={8}>
                    <Card className="appointmentForm">
                        <Card.Header as="h5">Patient's Entry Form</Card.Header>
                        <Card.Body>
                            <Form onSubmit={onFormSubmit} id="create-course-form">
                                <FormGroup role="form">
                                    <Input type="text" placeholder="Enter Todays Date" value={patientDetails.date} fieldname="Date" disabled />
                                    
                                    <Input type="text" placeholder="Enter Patient Name" name="name" value={patientDetails.name} fieldname="Patient Name" onChange={ e => onChange(e.target)} />
                                    {appointmentErrors.name.length > 0 &&
                                    <div className="errorMsg">{appointmentErrors.name}</div>
                                    }
                                    <label>Gender</label>
                                    {_gender()}
                                    {appointmentErrors.gender.length > 0 &&
                                    <div className="errorMsg">{appointmentErrors.gender}</div>
                                    }
                                    <Input type="number" placeholder="Enter Age" fieldname="Age" name="age" onChange={e => onChange(e.target)} value={patientDetails.age} />
                                    {appointmentErrors.age.length > 0 &&
                                        <div className="errorMsg">{appointmentErrors.age}</div>
                                    }
                                    <Input type="number" placeholder="Enter Patient Mobile Num" name="mobile" fieldname="Phone Number" onChange={e => onChange(e.target)} value={patientDetails.mobile} />
                                    {appointmentErrors.mobile.length > 0 &&
                                        <div className="errorMsg">{appointmentErrors.mobile}</div>
                                    }
                                    <Input type="text" placeholder="Enter Patient Full Address" name="address" fieldname="Address" onChange={e => onChange(e.target)} value={patientDetails.address} />
                                    {appointmentErrors.address.length > 0 &&
                                        <div className="errorMsg">{appointmentErrors.address}</div>
                                    }
                                    <label>Choose the Consult Department</label>
                                    <select className="ui dropdown fluid" name="department" onChange={_handleDepartmentChange} value={department}>
                                        <option value="0">Select Department</option>
                                        {departments.map((dept, index) => {
                                            return (
                                                <option key={index} value={dept.department}>
                                                    {dept.department} - {dept.description}
                                                </option>
                                            );
                                        })}
                                    </select>
                                    {appointmentErrors.department.length > 0 &&
                                        <div className="errorMsg">{appointmentErrors.department}</div>
                                    }
                                    <label>Choose the Consultant Doctor</label>
                                    <select className="ui dropdown fluid" name="doctorname" onChange={e => onChange(e.target)} value={patientDetails.consultdoctor}>
                                        <option value="0">Select Doctor to Consult</option>
                                        {filterDoctor && filterDoctor.map((doctor, index) => {
                                            return (
                                                <option key={index} value={doctor.doctorname}>
                                                    {doctor.doctorname}
                                                </option>
                                            );
                                        })}
                                    </select>
                                    {appointmentErrors.doctorname.length > 0 &&
                                        <div className="errorMsg">{appointmentErrors.doctorname}</div>
                                    }
                                    <Input type="text" placeholder="Enter Patient Email Id" name="email" fieldname="Email" onChange={e => onChange(e.target)} value={patientDetails.email} />
                                    {appointmentErrors.email.length > 0 &&
                                        <div className="errorMsg">{appointmentErrors.email}</div>
                                    }
                                </FormGroup>
                               
                                <Buttons name="Submit" type="submit"  onClick={onFormSubmit} />
                               
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                
                <Col col={4}>
                    <Col xs={6}>
                        <Button variant="primary" size="lg" block   className="patientbtn" onClick={_handleClick}><FaListOl /> List of Appointments </Button>
                        </Col>
                </Col>
               
            </Row>


        </div>

    )
}

export default Appointments;
;