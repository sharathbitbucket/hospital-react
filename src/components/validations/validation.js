import React from 'react';

const formInputValidations = (inputName, inputValue) =>
{
    const emailRegex = RegExp(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i);
    const mobileRegex = RegExp(/^[0-9]{10}$/i);
let errMessage = "";
switch(inputName)
{
    case "name":
    return inputValue.length === 0 ? "Name field Should not be empty": "";
    case "gender":
    return inputValue === "Select Gender" ? "Please Select Gender": "";
    case "age":
    return inputValue <= 0 || inputValue > 100 ? "Enter Valid Age": "";
    case "mobile":
    return mobileRegex.test(inputValue) ? "" : "Enter Valid Contact Number";
    case "address":
    return inputValue.length === 0 ? "address field Should not be empty": "";
    case "department":
    return inputValue === "Select Department" ? "department field Should not be empty": "";
    case "doctorname":
    return inputValue === "Select Doctor to Consult" ? "doctorname field Should not be empty": "";
    case "email":
        if (inputValue.length === 0) {
            errMessage = "Email Shouldn't Leave Empty"; 
          } else {
            errMessage = emailRegex.test(inputValue)? "" : "Enter Valid Email";
          }
          return errMessage;
    
    default:
    break;
}

}

export default formInputValidations;