import React from 'react';

function Cards(props) {
    return (
        <div class="ui cards" >
            <div class="ui card">
                <div class="content">
                    <div className={props.headerclass} >{props.header} {props.icon}</div>
                    <div className={props.metaclass}>{props.meta}</div>
                    <div className={props.descclass}>
                       <h3 onClick={props.onClick}>{props.description}</h3> 
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Cards;