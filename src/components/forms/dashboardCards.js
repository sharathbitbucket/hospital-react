import React from 'react';
import {Card} from 'react-bootstrap';

function DashboardCard (props)
{
    return (
        <>
        <Card bg={props.bg} text={props.text} className={props.className} style={{ width: '100%' }}>
            <Card.Header><h5>{props.header}</h5> {props.addbtn}</Card.Header>
            <Card.Body className={props.cardBody}>
            <Card.Title>{props.title}</Card.Title>
            <Card.Text>
                {props.content}
            </Card.Text>
            </Card.Body>
        </Card>
        </>
    )
}

export default DashboardCard;