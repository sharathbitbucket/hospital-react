import React from 'react';
import { Form } from "react-bootstrap";

function Input(props) {
   
    const { type, placeholder, name, onBlur, value, onChange } = props;
    
    return (


        <Form.Group controlId="formBasicEmail">
            <Form.Label>{props.fieldname}</Form.Label>
            <Form.Control type={type} placeholder={placeholder} name={name} as={props.as} onBlur={onBlur} value={value} onChange={onChange} />
        </Form.Group>

    )
}

export default Input;