import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import HospitalStoreReducer from "./reducers/index";
import PatientsSaga from "./saga/index";

function ConfigureStore() {
    const sagaRoot = createSagaMiddleware(); //

    const store = createStore(HospitalStoreReducer, applyMiddleware(sagaRoot));

    store.runSaga = sagaRoot.run(PatientsSaga);
    return store;
}

export default ConfigureStore;
